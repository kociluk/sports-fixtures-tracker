// https://firebase.google.com/docs/functions/typescript

import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import loadFlashScoreFixtures from './functions/loadFlashScoreFixtures';
import { RuntimeOptions } from './functions/types';

// Runtime options
const runtimeOpts: RuntimeOptions = { // @see https://firebase.google.com/docs/functions/manage-functions#set_runtime_options
    timeoutSeconds: 60, // 60 is default
    memory: '256MB', // 256MB is default
};

const config = functions.config(); // @see https://firebase.google.com/docs/functions/config-env
const functionsWithOptions = functions // @see https://firebase.google.com/docs/functions/locations
    .region(config.region.name)
    .runWith(runtimeOpts);

admin.initializeApp();

// @param snap @see https://firebase.google.com/docs/reference/functions/functions.database.DataSnapshot
// @param context @see https://firebase.google.com/docs/reference/functions/functions.EventContext

const cloudFunctions = {
    loadFlashScoreFixtures: loadFlashScoreFixtures(functionsWithOptions)
};

/**
 * SFT functions.
 */
exports.sft = cloudFunctions;
