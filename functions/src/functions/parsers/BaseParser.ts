/**
 * Created by Lukas Koci <lukas.koci@ackee.cz> on 23.11.2018.
 */

import { Fixture, ParserOptions } from '../types';
import * as https from 'https';

/**
 * BaseParser
 */
class BaseParser {

    debug: boolean;

    /** @param {Fixture[]} _fixtures */
    private _fixtures: Fixture[];

    /** @param {string[]} _fixtureUris */
    private _fixtureUris: string[];

    /** @param {string} _name */
    private readonly _name: string;

    /** @param {string} _sourceData */
    private _sourceData: string;

    /** @param {string} _sourceUri */
    private readonly _sourceUri: string;

    /**
     * @param {ParserOptions} options
     * @param {string} name
     */
    constructor(options: ParserOptions, name: string) {
        if (this.constructor === BaseParser) {
            throw new TypeError('Abstract class "BaseParser" cannot be instantiated directly.');
        }

        this.debug = options.debug || false;
        this._name = name;
        this._sourceUri = options.sourceUri;
    }

    /**
     * @return {Fixture[]}
     */
    get fixtures(): Fixture[] {
        return this._fixtures;
    }

    /**
     * @return {string}
     */
    get name(): string {
        return this._name;
    }

    /**
     * @return {string}
     */
    get sourceUri(): string {
        return this._sourceUri;
    }

    /**
     * @return {Promise<string>}
     */
    downloadSourceData(sourceUri: string): Promise<string> {
        // no 3rd party dependency needed
        // @see https://www.tomas-dvorak.cz/posts/nodejs-request-without-dependencies/
        return new Promise((resolve, reject) => {
            https.get(sourceUri, (res) => {

                // handle http errors
                if (res.statusCode !== 200) {
                    reject(new Error(`Failed to load page ${this.sourceUri}, status code: ${res.statusCode}`));
                }

                // temporary data holder
                const body = [];

                // push each chunk to the data array
                res.on('data', (chunk) => body.push(chunk));

                // resolve promise with joined chunks
                res.on('end', () => resolve(body.join('')));
            }).on('error', (err) => reject(err));  // handle connection errors of the request
        });
    }

    /**
     * @return {Promise<string[]>}
     */
    loadFixtureUris(): Promise<string[]> {
        // @todo load fixture Uris from Google spreadsheet
        this._fixtureUris = [
            'https://www.flashscore.com/team/mlada-boleslav/0f7GpAMu/fixtures/'
        ];

        return Promise.resolve(this._fixtureUris);
    }

}

export default BaseParser;
