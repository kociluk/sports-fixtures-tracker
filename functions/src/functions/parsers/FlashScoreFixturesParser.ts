/**
 * Created by Lukas Koci <lukas.koci@ackee.cz> on 23.11.2018.
 */

import BaseParser from './BaseParser';
import { Fixture, IParser, ParserOptions } from '../types';
import { identity } from '../utils';

// @todo replace console by real logger
const logger = console;

/**
 * FlashScoreFixturesParser
 * @extends BaseParser
 * @implements IParser
 */
class FlashScoreFixturesParser extends BaseParser implements IParser {

    /**
     * @param {ParserOptions} options
     * @param {string} name
     */
    constructor(options: ParserOptions, name: string = 'FlashScoreFixturesParser') {
        super(options, name);
    }

    parse() {
        const downloadAndStoreFixtures = (uri: string): Promise<Fixture[]> =>
            this.downloadSourceData(uri)
                .then(sourceData => {
                    // https://regexr.com/
                    const rowRegex = /(¬~AA÷)(.*?)(¬AL÷)/gi;

                    const data = {
                        matchId: {
                            parse: identity,
                            regex: /¬~AA÷(.*?)¬/i,
                        },
                        teamAway: {
                            parse: identity,
                            regex: /¬AF÷(.*?)¬/i,
                        },
                        teamHome: {
                            parse: identity,
                            regex: /¬AE÷(.*?)¬/i,
                        },
                        // time: {
                        //     parse: (timestamp) => (
                        //         (new Date(timestamp))
                        //             .toISOString()
                        //             .replace('T', ' ')
                        //             .slice(0, 19)
                        //     ),
                        //     regex: /¬AD÷(.*?)¬/i,
                        // },
                        timestamp: {
                            parse: parseInt,
                            regex: /¬AD÷(.*?)¬/i,
                        },
                    };

                    const fixtureTemplate: Fixture = {
                        matchId: 'matchId',
                        teamAway: 'teamAway',
                        teamHome: 'teamHome',
                        timestamp: 0
                    };

                    const fixtures = (sourceData.match(rowRegex) || []) // parse data to rows
                        .filter(identity)
                        .map(row => {
                            const fixture = Object.assign({}, fixtureTemplate);

                            Object.entries(data).map(([proName, ops]) => {
                                const matches = row.match(ops.regex);

                                Object.assign(fixture, { [proName]:  ops.parse(matches[1]) });
                            });

                            return fixture;
                        });

                    // @todo implement logging functions in BaseParser
                    logger.info(`Fixtures have been successfully parsed!`);
                    logger.debug({ fixtures: JSON.stringify(fixtures) });

                    return fixtures;
                });

        return this.loadFixtureUris()
            .then(fixtureUris =>
                Promise.all(fixtureUris.map(downloadAndStoreFixtures))
            )
            .catch(err => {
                logger.error(err.message);

                return [];
            });
    }
}

export default FlashScoreFixturesParser;
