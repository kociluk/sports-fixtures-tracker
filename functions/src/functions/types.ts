/**
 * Created by Lukas Koci <lukas.koci@ackee.cz> on 23.11.2018.
 */

export interface Fixture {
    matchId: string;
    teamAway: string;
    teamHome: string;
    timestamp: number;
}

export interface IParser {
    parse(): Promise<Fixture[][]>;
}

export interface ParserOptions {
    debug?: boolean,
    sourceUri: string;
}

export interface RuntimeOptions {
    timeoutSeconds: number;
    memory: '128MB' | '256MB' | '512MB' | '1GB' | '2GB';
}
