/**
 * Created by Lukas Koci <lukas.koci@ackee.cz> on 23.11.2018.
 */

import FlashScoreFixturesParser from './parsers/FlashScoreFixturesParser';

const loadFlashScoreFixtures = (functions, options = {}) =>
    functions.https.onRequest(
        async (req, res) => {
            const parser = new FlashScoreFixturesParser({
                sourceUri: 'spreadsheet' // @todo load from Google Spreadsheet
            });

            const fixtures = await parser.parse();
            // todo store fixtures to Firebase Database

            res.send(fixtures);
        }
    );

export default loadFlashScoreFixtures;
