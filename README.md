# Sports Fixtures Tracker

Easily Track Sports Fixtures Using Google Calendar!

## Cloud Functions for Firebase

Cloud Functions for Firebase lets you automatically run backend code in response to events triggered by Firebase features and HTTPS requests.
Your code is stored in Google's cloud and runs in a managed environment. 
There's no need to manage and scale your own servers.

Google documentation: https://firebase.google.com/docs/functions/

## How to deploy this project

1. Install **Firebase CLI** by running `npm install -g firebase-tools` (https://firebase.google.com/docs/cli/)
2. Set `<projectName>` in `.firebaser` for all environments (`./.firebaserc`)
3. Set `<projectName>` in `package.json` (name and description) (`./functions/package.json`)
4. Deploy Cloud functions using deployment scripts by running `npm run deploy`
